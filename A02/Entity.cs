namespace A02
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class Entity : DbContext
    {
        public Entity()
           : base("name=Entity")
        {
        }

        public DbSet<Student> students { set; get; }

    }
}