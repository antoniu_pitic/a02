﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace A02
{
    public partial class Cell : UserControl
    {
        public Cell()
        {
            InitializeComponent();
        }

        private void Cell_Click(object sender, EventArgs e)
        {
            BackColor = Color.Red;
        }
    }
}
