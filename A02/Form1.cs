﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace A02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Student student = new Student
            {
                name = "Pitic",
                surname = "Antoniu",
                orasId = 1,
                studentId = 1

            };

            var db = new Entity();

            db.students.Add(student);
            db.students.Add(student);
            db.students.Add(student);
            db.SaveChanges();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f = new A02.Form2();
            f.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 5; i++)
            {
                Cell t = new A02.Cell();
                t.Location = new Point(20 + 80 * i, 170);
                t.label1.Text = i.ToString();
                t.label2.Text = "!";
                Controls.Add(t);
            }

        }

        private void g(object sender, EventArgs e)
        {
            ((Label)sender).Parent.BackColor = Color.Red;
        }

        private void f(object sender, EventArgs e)
        {
            ((Panel)sender).BackColor = Color.Red;
        }
    }
}
